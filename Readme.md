Technologies:

* C#
* .Net 5
* MongoDB
* Redis

### For starting the server with docker-compose
```zsh
    docker-compose up --build
```
* There are related app keys already in appsetting.json

### Your server will run at http://localhost:5005
* You can go to Swagger with this Url: http://localhost:5005/swagger/index.html

### Notes
* There is two user role which are 'Customer' and 'Admin'
  * Admin user is creating with seed data when initializing application <br />
    email: admin@admin.com <br />
    password: psswrd     
  * All users can access the /api/userAccount/myInfos endpoint
  * All new users are 'Customer'
  * Admin user can access the /api/products endpoints <br />
    Can see the all products and update product stock amount
  * Customer user can access the /api/order endpoints <br />
    Can create order, see order details and see own all orders
* Products and Admin user data are inserting when project initializing
* Used Mongo transaction feature for providing the stock amount consistency
  * For using mongo transaction feature,  must using the Mongo with clustered structure because Mongo not support transaction feature for standalone structure.
* For logging the entity changes, Mongo events are listened for some commands 
* Used Redis for caching. Only user infos cached in Redis for showing the usage example
* Only was written the LoginUserAsync method's test schenarios with XUnit for showing the test example
* All errors are catching with error middleware
* JWT was token used for secure endpoint