﻿using System;

namespace ReadingIsGood.Libraries.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message)
        { }
    }
}