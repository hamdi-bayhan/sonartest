﻿using System;
using System.Linq;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver.Core.Events;
using ReadingIsGood.Data.Entities;
using ReadingIsGood.Data;
using ReadingIsGood.Dto.Common;

namespace ReadingIsGood.Libraries
{
    public class MongoEntityLogs : IEventSubscriber
    {
        private readonly ILogger logger;
        private readonly ReflectionEventSubscriber subscriber;
        private readonly string[] commandNameArr = {
                DbLogCommandNameConstants.DELETE,
                DbLogCommandNameConstants.FINDANDMODIFY,
                DbLogCommandNameConstants.INSERT
            };
        private string CurrentUserId { get; set; }

        public MongoEntityLogs(ILogger logger, string currentUserId)
        {
            this.logger = logger;
            subscriber = new ReflectionEventSubscriber(this);
            CurrentUserId = currentUserId;
        }
        public bool TryGetEventHandler<TEvent>(out Action<TEvent> handler)
        {
            return subscriber.TryGetEventHandler(out handler);
        }

        public void Handle(CommandStartedEvent e)
        {
            if (commandNameArr.Contains(e.CommandName))
                logger.LogInformation(GetStartedEventLogObj(e).ToJson());
        }

        public void Handle(CommandSucceededEvent e)
        {
            if (commandNameArr.Contains(e.CommandName))
                logger.LogInformation(GetSucceededEventLogObj(e).ToJson());
        }


        private DbLogStartedEvent GetStartedEventLogObj(CommandStartedEvent e)
        {
            var logObject = new DbLogStartedEvent
            {
                UserId = CurrentUserId,
                RequestId = e.RequestId,
                CommandName = e.CommandName
            };

            switch (e.CommandName)
            {
                case DbLogCommandNameConstants.FINDANDMODIFY:
                    logObject.CollectionName = (string)e.Command[DbLogCommandNameConstants.FINDANDMODIFY];
                    logObject.Data = e.Command["update"].ToJson();
                    break;
                case DbLogCommandNameConstants.INSERT:
                    logObject.CollectionName = (string)e.Command[DbLogCommandNameConstants.INSERT];
                    var data = e.Command["documents"][0];
                    SetDataToLogObjectForInsertMethod(ref logObject, data);
                    break;
                default:
                    break;
            }

            return logObject;
        }

        private DbLogSucceededEvent GetSucceededEventLogObj(CommandSucceededEvent e)
        {
            var logObject = new DbLogSucceededEvent();
            logObject.UserId = CurrentUserId;
            logObject.RequestId = e.RequestId;
            logObject.CommandName = e.CommandName;
            logObject.Duration = e.Duration;

            return logObject;
        }

        private void SetDataToLogObjectForInsertMethod(ref DbLogStartedEvent logObject, BsonValue data)
        {
            if (logObject.CollectionName == "User" && data["Password"] != null)
            {
                var uObject = new User()
                {
                    Email = (string)data["Email"],
                    Age = (int)data["Age"],
                    Role = (string)data["Role"],
                    Name = (string)data["Name"],
                    CreatedAt = (DateTime)data["CreatedAt"],
                };
                uObject.SetId((string)data["_id"]);

                logObject.UserId = uObject.Id;
                logObject.Data = uObject.ToJson();
                CurrentUserId = uObject.Id;
            }
            else
            {
                logObject.Data = data.ToJson();
            }
        }
    }
}

