using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using ReadingIsGood.Dto.ApiResponse;
using ReadingIsGood.Helpers;
using ReadingIsGood.Libraries.Exceptions;
using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace ReadingIsGood.Libraries
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ILogger<ExceptionMiddleware> logger;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            this.next = next;
            this.logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await next(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex, logger);
            }
        }

        private Task HandleExceptionAsync(HttpContext httpContext, Exception ex, ILogger logger)
        {
            var options = new JsonSerializerOptions();
            options.Converters.Add(new ExceptionConverter());

            SetStatusAndResponse(ex, out HttpStatusCode status, out Response<string> response);

            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)status;

            logger.LogError(ex, ex.Message);

            var result = JsonSerializer.Serialize(response, options);
            return httpContext.Response.WriteAsync(result);
        }

        private void SetStatusAndResponse(Exception exception, out HttpStatusCode status, out Response<string> response)
        {
            response = Response<string>.GetError(exception, exception.Message);
            switch (exception)
            {
                case BadRequestException ex:
                    status = HttpStatusCode.BadRequest;
                    break;
                case NotFoundException ex:
                    status = HttpStatusCode.NotFound;
                    break;
                default:
                    status = HttpStatusCode.InternalServerError;
                    response = Response<string>.GetError(null, "An error occured");
                    break;
            }
        }
    }
}