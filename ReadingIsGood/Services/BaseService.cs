﻿using ReadingIsGood.Data.Base;
using ReadingIsGood.Interfaces;
using ReadingIsGood.Helpers;
using Microsoft.AspNetCore.Http;

namespace ReadingIsGood.Services
{
    public class BaseService<T> : IBaseService<T> where T : BaseEntity
    {
        private readonly IHttpContextAccessor httpContext;
        public string _CurrentUserId { get; set; }

        public BaseService(IHttpContextAccessor httpContext)
        {
            this.httpContext = httpContext;

            var user = httpContext?.HttpContext?.User;
            if(user != null) _CurrentUserId = user.GetId();
        }
    }
}