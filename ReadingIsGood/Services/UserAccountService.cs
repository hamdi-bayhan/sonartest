﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using ReadingIsGood.Data.Entities;
using ReadingIsGood.Data.Interfaces;
using ReadingIsGood.Dto.ApiRequest;
using ReadingIsGood.Dto.ApiResponse;
using ReadingIsGood.Dto.Cache;
using ReadingIsGood.Interfaces;
using ReadingIsGood.Data;
using ReadingIsGood.Libraries.Exceptions;
using static ReadingIsGood.Data.Cache.Redis.RedisDb;
using BC = BCrypt.Net.BCrypt;

namespace ReadingIsGood.Services
{
    public class UserAccountService : BaseService<User>, IUserAccountService
    {
        private readonly IUserRepository userRepository;
        private readonly IMapper mapper;
        private readonly ICacheService cacheService;
        private readonly IJwtTokenService jwtTokenService;

        public UserAccountService(
            IHttpContextAccessor httpContext,
            IUserRepository userRepository,
            ICacheService cacheService,
            IJwtTokenService jwtTokenService,
            IMapper mapper) : base (httpContext)
        {
            this.userRepository = userRepository;
            this.mapper = mapper;
            this.cacheService = cacheService;
            this.jwtTokenService = jwtTokenService;
        }

        public async Task RegisterUserAsync(RegisterParams model)
        {
            var result = await userRepository.GetByEmailAsync(model.Email);
            if (result != null)
                throw new BadRequestException("User already exist");

            var user = GetRegisterUserObj(model);

            await userRepository.AddAsync(user);
            SetUserInfoToCache(user);
        }

        public async Task<LoginResponse> LoginUserAsync(LoginParams model)
        {
            var result = await userRepository.GetByEmailAsync(model.Email);

            if (result == null || !BC.Verify(model.Password, result.Password))
                throw new BadRequestException("Email or Password is incorrect");

            return new LoginResponse() { Token = jwtTokenService.GetLoginToken(result) };
        }

        public async Task<UserInfoResponse> GetCurrentUserInfoAsync()
        {
            User result;
            var userInfoRedis = cacheService.GetServiceDb((int)RedisDbNumbers.UserInfo)
                                            .GetString(_CurrentUserId);

            if (userInfoRedis.HasValue)
                result = JsonSerializer.Deserialize<User>(userInfoRedis);
            else
            {
                result = await userRepository.GetByIdAsync(_CurrentUserId);
                SetUserInfoToCache(result);
            }

            return mapper.Map<UserInfoResponse>(result);
        }

        private void SetUserInfoToCache(User user)
        {
            var userInfo = mapper.Map<UserInfoCache>(user);

            cacheService.GetServiceDb((int)RedisDbNumbers.UserInfo)
                .SetString(user.Id, JsonSerializer.Serialize(userInfo));
        }

        private User GetRegisterUserObj(RegisterParams model)
        {
            return new User
            {
                Email = model.Email,
                Password = BC.HashPassword(model.Password),
                Name = model.Name,
                Age = model.Age,
                Role = UserRoleConstants.CUSTOMER,
                CreatedAt = DateTime.Now
            };
        }
    }
}