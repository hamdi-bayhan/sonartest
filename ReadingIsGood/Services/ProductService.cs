﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using ReadingIsGood.Data.Entities;
using ReadingIsGood.Data.Interfaces;
using ReadingIsGood.Dto.ApiRequest;
using ReadingIsGood.Dto.ApiResponse;
using ReadingIsGood.Interfaces;
using ReadingIsGood.Libraries.Exceptions;

namespace ReadingIsGood.Services
{
    public class ProductService : BaseService<Product>, IProductService
    {
        private readonly IProductRepository productRepository;
        private readonly IMapper mapper;

        public ProductService(
            IHttpContextAccessor httpContext,
            IProductRepository productRepository,
            IMapper mapper) : base(httpContext)
        {
            this.productRepository = productRepository;
            this.mapper = mapper;
        }

        public async Task<ProductResponse> UpdateStockAmountAsync(
            UpdateStockAmountParams model, string productId)
        {
            var product = new Product() { };
            product.StockAmount = model.StockAmount;
            product.SetId(productId);

            var result = await productRepository.FindOneAndReplaceAsync(product);
            if (result == null) throw new NotFoundException("Product not found");

            return mapper.Map<ProductResponse>(result);
        }

        public async Task<List<ProductResponse>> GetAllProductsAsync()
        {
            var result = await productRepository.GetAllAsync();
            return mapper.Map<List<ProductResponse>>(result.ToList());
        }
    }
}