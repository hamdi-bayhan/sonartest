﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using ReadingIsGood.Data.Entities;
using ReadingIsGood.Data.Interfaces;
using ReadingIsGood.Dto.ApiRequest;
using ReadingIsGood.Dto.ApiResponse;
using ReadingIsGood.Interfaces;
using ReadingIsGood.Libraries.Exceptions;
using static ReadingIsGood.Data.Enums;

namespace ReadingIsGood.Services
{
    public class OrderService : BaseService<Order>, IOrderService
    {
        private readonly IOrderRepository orderRepository;
        private readonly IProductRepository productRepository;
        private readonly IMapper mapper;

        public OrderService(
            IHttpContextAccessor httpContext,
            IOrderRepository orderRepository,
            IProductRepository productRepository,
            IMapper mapper) : base( httpContext )
        {
            this.orderRepository = orderRepository;
            this.productRepository = productRepository;
            this.mapper = mapper;
        }

        public async Task CreateOrderAsync(CreateOrderParams model)
        {
            var mongoClient = orderRepository.GetMongoClient();
            using var Session = await mongoClient.StartSessionAsync();
            Session.StartTransaction();

            try
            {
                foreach (var item in model.OrderItemsRequest)
                {
                    var product = await productRepository.GetByIdAsync(Session, item.ProductId);
                    IsOrderItemValid(product, item);

                    product.StockAmount -= item.Amount;
                    await productRepository.FindOneAndReplaceAsync(Session, product);
                }

                var order = GetToBeCreatedOrderObj(model);
                await orderRepository.AddAsync(Session, order);

                await Session.CommitTransactionAsync();
            }
            catch (Exception)
            {
                await Session.AbortTransactionAsync();
                throw;
            }
        }

        private Order GetToBeCreatedOrderObj(CreateOrderParams order)
        {
            return new Order
            {
                UserId = _CurrentUserId,
                OrderItems = mapper.Map<List<OrderItem>>(order.OrderItemsRequest),
                Status = (int)OrderStatus.NewOrder,
                CreatedAt = DateTime.Now
            };
        }

        private void IsOrderItemValid(Product product, OrderItemRequest item)
        {
            if (product == null)
                throw new NotFoundException($"Product not found with {item.ProductId} ID");

            if (product.StockAmount < item.Amount)
                throw new BadRequestException($"Product stock amount not enough with {item.ProductId} ID");
        }

        public async Task<OrderResponse> GetOrderDetailAsync(string orderId)
        {
            var order = await orderRepository.GetByIdAsync(orderId);
            if (order == null)
                throw new NotFoundException($"Order not found with {orderId} ID");

            return mapper.Map<OrderResponse>(order);
        }

        public async Task<List<OrderResponse>> GetCurrentUserOrdersAsync()
        {
            var orders = await orderRepository.GetCurrentUserOrdersAsync(_CurrentUserId);
            return mapper.Map<List<Order>, List<OrderResponse>>(orders);
        }
    }
}