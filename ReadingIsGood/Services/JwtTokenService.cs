﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ReadingIsGood.Data.Entities;
using ReadingIsGood.Interfaces;

namespace ReadingIsGood.Services
{
    public class JwtTokenService : IJwtTokenService
    {
        public static IConfiguration config = Startup.StaticConfig;

        public string GetLoginToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var claims = GetListClaims(user);
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["JwtToken:Key"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = GetTokenDescriptor(claims, credentials);
            var tokenObject = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenObject != null ? tokenHandler.WriteToken(tokenObject) : null;

            return token;
        }

        private List<Claim> GetListClaims(User user)
        {
            return new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.GivenName, user.Name),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(ClaimTypes.Role, user.Role)
            };
        }

        private SecurityTokenDescriptor GetTokenDescriptor(List<Claim> claims, SigningCredentials credentials)
        {
            var expireHour = Convert.ToDouble(config["JwtToken:ExpireHour"]);

            return new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddHours(expireHour),
                SigningCredentials = credentials
            };
        }
    }
}
