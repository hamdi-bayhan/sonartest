﻿using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReadingIsGood.Dto.ApiRequest;
using ReadingIsGood.Dto.ApiResponse;
using ReadingIsGood.Interfaces;
using ReadingIsGood.Data;

namespace ReadingIsGood.Controllers
{
    [Authorize(Roles = UserRoleConstants.CUSTOMER)]
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService orderService;

        public OrderController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        // POST api/order/createOrder
        [HttpPost("createOrder")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> CreateOrder([FromBody] CreateOrderParams model)
        {
            await orderService.CreateOrderAsync(model);
            return Created("", Response<OrderResponse>.GetSuccess(null, "Order created as successfuly"));
        }

        // GET api/order/orderDetail/{orderId}
        [HttpGet("orderDetail/{orderId}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> OrderDetail([FromRoute] string orderId)
        {
            var result = await orderService.GetOrderDetailAsync(orderId);
            return Ok(Response<OrderResponse>.GetSuccess(result, "Order returned as successfuly"));
        }

        // GET api/order/allOrders
        [HttpGet("allOrders")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> CurrentUserOrders()
        {
            var result = await orderService.GetCurrentUserOrdersAsync();
            return Ok(Response<List<OrderResponse>>.GetSuccess(result));
        }
    }
}
