﻿using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReadingIsGood.Dto.ApiRequest;
using ReadingIsGood.Dto.ApiResponse;
using ReadingIsGood.Interfaces;
using ReadingIsGood.Data;

namespace ReadingIsGood.Controllers
{
    [Authorize(Roles = UserRoleConstants.ADMIN)]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        // POST api/product/updateStockAmount
        [HttpPost("updateStockAmount/{productId}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> UpdateStockAmount(
            [FromBody] UpdateStockAmountParams model,
            [FromRoute] string productId)
        {
            var result = await productService.UpdateStockAmountAsync(model, productId);
            return Ok(Response<ProductResponse>
                .GetSuccess(result, "Product stock amount updated successfully"));
        }

        // GET api/product/allProducts
        [HttpGet("allProducts")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> AllProducts()
        {
            var result = await productService.GetAllProductsAsync();
            return Ok(Response<List<ProductResponse>>.GetSuccess(result));
        }
    }
}
