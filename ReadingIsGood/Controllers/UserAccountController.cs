﻿using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReadingIsGood.Dto.ApiRequest;
using ReadingIsGood.Dto.ApiResponse;
using ReadingIsGood.Interfaces;

namespace ReadingIsGood.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAccountController : ControllerBase
    {
        private readonly IUserAccountService userAccountService;

        public UserAccountController(IUserAccountService userAccountService)
        {
            this.userAccountService = userAccountService;
        }

        // POST api/userAccount/register
        [HttpPost("register")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> Register([FromBody] RegisterParams model)
        {
            await userAccountService.RegisterUserAsync(model);
            return Created("", Response<object>.GetSuccess(null, "User created as successfully"));
        }

        // POST api/userAccount/login
        [HttpPost("login")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> Login([FromBody] LoginParams model)
        {
            var result = await userAccountService.LoginUserAsync(model);
            return Ok(Response<LoginResponse>.GetSuccess(result));
        }

        // GET api/userAccount/myInfos
        [Authorize]
        [HttpGet("myInfos")]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> UserInfo()
        {
            var result = await userAccountService.GetCurrentUserInfoAsync();
            return Ok(Response<UserInfoResponse>.GetSuccess(result));
        }
    }
}