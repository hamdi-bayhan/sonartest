﻿using AutoMapper;
using ReadingIsGood.Data.Entities;
using ReadingIsGood.Dto.ApiRequest;
using ReadingIsGood.Dto.ApiResponse;

namespace ReadingIsGood.Dto.Mapping
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<Order, OrderResponse>();
            CreateMap<OrderItemRequest, OrderItem>();
            CreateMap<OrderItem, OrderItemRequest>();
        }
    }
}
