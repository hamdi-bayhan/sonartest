﻿using AutoMapper;
using ReadingIsGood.Data.Entities;
using ReadingIsGood.Dto.ApiResponse;
using ReadingIsGood.Dto.Cache;

namespace ReadingIsGood.Dto.Mapping
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserInfoResponse>();
            CreateMap<User, UserInfoCache>();
        }
    }
}
