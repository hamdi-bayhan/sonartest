﻿using AutoMapper;
using ReadingIsGood.Data.Entities;
using ReadingIsGood.Dto.ApiResponse;

namespace ReadingIsGood.Dto.Mapping
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, ProductResponse>();
        }
    }
}
