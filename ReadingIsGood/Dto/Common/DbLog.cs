﻿using System;
using ReadingIsGood.Data;

namespace ReadingIsGood.Dto.Common
{
    public class DbLogBaseObject
    {
        public string UserId { get; set; }
        public int RequestId { get; set; }
        public string CommandName { get; set; }
    }

    public class DbLogStartedEvent : DbLogBaseObject
    {
        public string EventType { get; set; } = DbLogEventTypeConstants.STARTED;
        public string CollectionName { get; set; }
        public dynamic Data { get; set; }
    }

    public class DbLogSucceededEvent : DbLogBaseObject
    {
        public string EventType { get; set; } = DbLogEventTypeConstants.SUCCEEDED;
        public TimeSpan Duration { get; set; }
    }
}
