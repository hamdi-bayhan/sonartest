﻿using System;
using System.Collections.Generic;
using ReadingIsGood.Data.Entities;

namespace ReadingIsGood.Dto.ApiResponse
{
    public class OrderResponse
    {
        public string Id { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public int Status { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}