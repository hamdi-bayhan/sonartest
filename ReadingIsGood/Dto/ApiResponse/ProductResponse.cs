﻿namespace ReadingIsGood.Dto.ApiResponse
{
    public class ProductResponse
    {
        public string Id { get; set; }
        public int StockAmount { get; set; }
    }
}