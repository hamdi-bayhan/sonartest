﻿using System;

namespace ReadingIsGood.Dto.ApiResponse
{
    public class UserInfoResponse
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}