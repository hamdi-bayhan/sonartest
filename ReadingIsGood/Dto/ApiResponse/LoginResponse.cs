﻿namespace ReadingIsGood.Dto.ApiResponse
{
    public class LoginResponse
    {
        public string Token { get; set; }
    }
}