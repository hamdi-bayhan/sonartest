﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReadingIsGood.Dto.ApiRequest
{
    public class CreateOrderParams
    {
        [Required]
        public List<OrderItemRequest> OrderItemsRequest { get; set; }
    }

    public class OrderItemRequest
    {
        [Required]
        public string ProductId { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int Amount { get; set; }
    }
}