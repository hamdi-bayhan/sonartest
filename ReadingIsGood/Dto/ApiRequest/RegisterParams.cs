﻿using System.ComponentModel.DataAnnotations;

namespace ReadingIsGood.Dto.ApiRequest
{
    public class RegisterParams
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Please enter a valid email")]
        public string Email { get; set; }

        [Required]
        [StringLength(255, ErrorMessage = "Must be between 5 and 255 characters", MinimumLength = 5)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [StringLength(255, ErrorMessage = "Must be between 5 and 255 characters", MinimumLength = 5)]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        public int Age { get; set; }
    }
}