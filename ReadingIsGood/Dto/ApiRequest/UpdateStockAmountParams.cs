﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReadingIsGood.Dto.ApiRequest
{
    public class UpdateStockAmountParams
    {
        [Required]
        public int StockAmount { get; set; }
    }
}