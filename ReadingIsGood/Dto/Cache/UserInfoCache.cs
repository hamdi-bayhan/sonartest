﻿using System;

namespace ReadingIsGood.Dto.Cache
{
    public class UserInfoCache
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Role { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
