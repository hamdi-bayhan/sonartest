﻿using System;
using ReadingIsGood.Data.Base;

namespace ReadingIsGood.Data.Entities
{
    public class User : BaseEntity
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Role { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}