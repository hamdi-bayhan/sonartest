﻿using System;
using ReadingIsGood.Data.Base;
using System.Collections.Generic;

namespace ReadingIsGood.Data.Entities
{
    public class Order : BaseEntity
    {
        public string UserId { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public int Status { get; set; }
        public DateTime CreatedAt { get; set; }
    }

    public class OrderItem
    {
        public string ProductId { get; set; }
        public int Amount { get; set; }
    }
}