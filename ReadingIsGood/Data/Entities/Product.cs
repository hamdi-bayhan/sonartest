﻿using ReadingIsGood.Data.Base;

namespace ReadingIsGood.Data.Entities
{
    public class Product : BaseEntity
    {
        public int StockAmount { get; set; }
    }
}