﻿namespace ReadingIsGood.Data
{
    static class UserRoleConstants
    {
        public const string ADMIN = "Admin";
        public const string CUSTOMER = "Customer";
    }

    static class DbLogEventTypeConstants
    {
        public const string STARTED = "Started";
        public const string SUCCEEDED = "Succeeded";
        public const string FAILED = "Failed";
    }

    static class DbLogCommandNameConstants
    {
        public const string INSERT = "insert";
        public const string FINDANDMODIFY = "findAndModify";
        public const string DELETE = "delete";
    }
}
