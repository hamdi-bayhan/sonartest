﻿using System;
using System.Linq;
using ReadingIsGood.Data.Entities;
using ReadingIsGood.Data.Interfaces;
using ReadingIsGood.Data;
using BC = BCrypt.Net.BCrypt;

namespace ReadingIsGood.Data
{
    public static class SeedData
    {
        public static void SeedAllData(
            IUserRepository userRepository,
            IProductRepository productRepository)
        {
            SeedAdminData(userRepository);
            SeedProductData(productRepository);
        }

        private static void SeedAdminData(IUserRepository userRepository)
        {
            var adminEmail = "admin@admin.com";

            var adminData = userRepository.GetByEmailAsync(adminEmail).GetAwaiter().GetResult();
            if (adminData != null) return;

            var adminUser = new User()
            {
                Email = adminEmail,
                Password = BC.HashPassword("psswrd"),
                Role = UserRoleConstants.ADMIN,
                Name = "admin",
                Age = 99,
                CreatedAt = DateTime.Now
            };

            userRepository.AddAsync(adminUser).GetAwaiter().GetResult();
        }

        private static void SeedProductData(IProductRepository productRepository)
        {
            var products = productRepository.GetAllAsync().GetAwaiter().GetResult();
            if (products.Any()) return;

            for (int i = 0; i < 3; i++)
            {
                var product = new Product() { StockAmount = i + 5 };
                productRepository.AddAsync(product).GetAwaiter().GetResult();
            }
        }
    }
}
