﻿using ReadingIsGood.Data.Entities;
using ReadingIsGood.Data.Interfaces;

namespace ReadingIsGood.Data.Repository
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(IMongoContext context) : base(context)
        {
        }
    }
}