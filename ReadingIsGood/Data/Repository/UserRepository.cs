﻿using System.Threading.Tasks;
using MongoDB.Driver;
using ReadingIsGood.Data.Entities;
using ReadingIsGood.Data.Interfaces;

namespace ReadingIsGood.Data.Repository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(IMongoContext context) : base(context)
        {
        }

        public async Task<User> GetByEmailAsync(string email)
        {
            var filter = Builders<User>.Filter.Eq(f => f.Email, email);
            return await DbSet.Find(filter).FirstOrDefaultAsync();
        }
    }
}