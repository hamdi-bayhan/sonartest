﻿using MongoDB.Driver;
using ReadingIsGood.Data.Base;
using ReadingIsGood.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ReadingIsGood.Data.Repository
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly IMongoContext Context;
        protected IMongoCollection<TEntity> DbSet;
        protected MongoClient MongoClient;

        protected BaseRepository(IMongoContext context)
        {
            Context = context;
            DbSet = Context.GetCollection<TEntity>(typeof(TEntity).Name);
            MongoClient = Context.GetMongoClient();
        }

        public virtual async Task AddAsync(TEntity obj) =>
            await DbSet.InsertOneAsync(obj);

        public virtual async Task AddAsync(IClientSessionHandle session, TEntity obj) =>
            await DbSet.InsertOneAsync(session, obj);

        public virtual async Task<TEntity> GetByIdAsync(string id)
        {
            var data = await DbSet.FindAsync(Builders<TEntity>.Filter.Eq("_id", id));
            return data.SingleOrDefault();
        }

        public virtual async Task<TEntity> GetByIdAsync(IClientSessionHandle session, string id)
        {
            var data = await DbSet.FindAsync(session, Builders<TEntity>.Filter.Eq("_id", id));
            return data.SingleOrDefault();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            var all = await DbSet.FindAsync(Builders<TEntity>.Filter.Empty);
            return all.ToList();
        }

        public virtual async Task UpdateAsync(TEntity obj)
        {
            Expression<Func<TEntity, string>> func = f => f.Id;
            var value = (string)obj.GetType().GetProperty(func.Body.ToString().Split(".")[1]).GetValue(obj, null);
            var filter = Builders<TEntity>.Filter.Eq(func, value);

            await DbSet.ReplaceOneAsync(filter, obj);
        }

        public virtual async Task UpdateAsync(IClientSessionHandle session, TEntity obj)
        {
            Expression<Func<TEntity, string>> func = f => f.Id;
            var value = (string)obj.GetType().GetProperty(func.Body.ToString().Split(".")[1]).GetValue(obj, null);
            var filter = Builders<TEntity>.Filter.Eq(func, value);

            await DbSet.ReplaceOneAsync(session, filter, obj);
        }

        public virtual async Task<TEntity> FindOneAndReplaceAsync(TEntity obj)
        {
            Expression<Func<TEntity, string>> func = f => f.Id;
            var value = (string)obj.GetType().GetProperty(func.Body.ToString().Split(".")[1]).GetValue(obj, null);
            var filter = Builders<TEntity>.Filter.Eq(func, value);
            var options = new FindOneAndReplaceOptions<TEntity, TEntity>
            {
                ReturnDocument = ReturnDocument.After
            };

            return await DbSet.FindOneAndReplaceAsync(filter, obj, options);
        }

        public virtual async Task<TEntity> FindOneAndReplaceAsync(IClientSessionHandle session, TEntity obj)
        {
            Expression<Func<TEntity, string>> func = f => f.Id;
            var value = (string)obj.GetType().GetProperty(func.Body.ToString().Split(".")[1]).GetValue(obj, null);
            var filter = Builders<TEntity>.Filter.Eq(func, value);
            var options = new FindOneAndReplaceOptions<TEntity, TEntity>
            {
                ReturnDocument = ReturnDocument.After
            };

            return await DbSet.FindOneAndReplaceAsync(session, filter, obj, options);
        }

        public virtual async Task RemoveAsync(string id) =>
            await DbSet.DeleteOneAsync(Builders<TEntity>.Filter.Eq("_id", id));

        public virtual MongoClient GetMongoClient()
        {
            return MongoClient;
        }

        public void Dispose()
        {
            Context?.Dispose();
        }
    }
}