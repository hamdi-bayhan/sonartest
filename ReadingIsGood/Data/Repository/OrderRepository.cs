﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using ReadingIsGood.Data.Entities;
using ReadingIsGood.Data.Interfaces;

namespace ReadingIsGood.Data.Repository
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(IMongoContext context) : base(context)
        {
        }

        public async Task<List<Order>> GetCurrentUserOrdersAsync(string id)
        {
            var data = await DbSet.FindAsync(Builders<Order>.Filter.Eq("UserId", id));
            return data.ToList();
        }
    }
}