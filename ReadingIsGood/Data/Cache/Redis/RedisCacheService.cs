﻿using System;
using ReadingIsGood.Data.Interfaces;
using StackExchange.Redis;

namespace ReadingIsGood.Data.Cache.Redis
{
    public class RedisCacheService : ICacheService
    {
        private readonly RedisServer redisServer;
        private IDatabase database;

        public RedisCacheService(RedisServer redisServer)
        {
            this.redisServer = redisServer;
            database = redisServer.Database;
        }

        public ICacheService GetServiceDb(int dbNumber)
        {
            SetDB(dbNumber);
            return this;
        }

        private void SetDB(int dbNumber) =>
            database = redisServer.GetDatabase(dbNumber);

        public void SetString(RedisKey key, string value) =>
            database.StringSet(key, value);

        public RedisValue GetString(RedisKey key)
        {
            return database.StringGet(key);
        }
    }
}