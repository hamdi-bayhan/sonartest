﻿using Microsoft.Extensions.Configuration;
using StackExchange.Redis;

namespace ReadingIsGood.Data.Cache.Redis
{
    public class RedisServer
    {
        private readonly ConnectionMultiplexer connectionMultiplexer;
        private readonly IDatabase database;
        private string configurationString;

        public RedisServer(IConfiguration configuration)
        {
            SetRedisConfigurationString(configuration);
            connectionMultiplexer = ConnectionMultiplexer.Connect(configurationString);
        }

        public IDatabase Database => database;

        public IDatabase GetDatabase(int dbNumber)
        {
            return connectionMultiplexer.GetDatabase(dbNumber);
        }

        private void SetRedisConfigurationString(IConfiguration configuration)
        {
            string host = configuration["RedisConfiguration:Host"];
            string port = configuration["RedisConfiguration:Port"];
            string password = configuration["RedisConfiguration:Password"];
            configurationString = $"{host}:{port},password={password}";
        }
    }
}