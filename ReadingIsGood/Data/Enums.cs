﻿using System.ComponentModel;

namespace ReadingIsGood.Data
{
    public class Enums
    {
        public enum OrderStatus : int
        {
            [Description("New Order")]
            NewOrder = 0,
            [Description("Completed Order")]
            CompletedOrder = 1
        }
    }
}