﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using ReadingIsGood.Data.Interfaces;
using ReadingIsGood.Helpers;
using ReadingIsGood.Libraries;
using System;

namespace ReadingIsGood.Data.Context
{
    public class MongoContext : IMongoContext
    {
        private IMongoDatabase Database { get; set; }
        public MongoClient MongoClient { get; set; }
        public string CurrentUserId { get; set; }

        private readonly IConfiguration configuration;
        private readonly ILogger<MongoContext> logger;
        private readonly IHttpContextAccessor httpContext;

        public MongoContext(
            IConfiguration configuration,
            ILogger<MongoContext> logger,
            IHttpContextAccessor httpContext)
        {
            this.configuration = configuration;
            this.logger = logger;
            this.httpContext = httpContext;

            var user = httpContext?.HttpContext?.User;
            if (user != null) CurrentUserId = user.GetId();
        }

        private void ConfigureMongo()
        {
            if (MongoClient != null)
                return;

            var mongoConnectionUrl = new MongoUrl(configuration["MongoDBSettings:ConnectionString"]);
            var mongoClientSettings = MongoClientSettings.FromUrl(mongoConnectionUrl);
            mongoClientSettings.ClusterConfigurator = cb =>
               cb.Subscribe(new MongoEntityLogs(logger, CurrentUserId));

            MongoClient = new MongoClient(mongoClientSettings);

            Database = MongoClient.GetDatabase(configuration["MongoDBSettings:DatabaseName"]);
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            ConfigureMongo();

            return Database.GetCollection<T>(name);
        }

        public MongoClient GetMongoClient()
        {
            ConfigureMongo();

            return MongoClient;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}