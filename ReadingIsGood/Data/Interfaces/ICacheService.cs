﻿using StackExchange.Redis;

namespace ReadingIsGood.Data.Interfaces
{
    public interface ICacheService
    {
        void SetString(RedisKey key, string value);
        RedisValue GetString(RedisKey key);
        ICacheService GetServiceDb(int dbNumber);
    }
}