﻿using ReadingIsGood.Data.Entities;

namespace ReadingIsGood.Data.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}