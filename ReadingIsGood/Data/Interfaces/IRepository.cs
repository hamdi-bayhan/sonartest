﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace ReadingIsGood.Data.Interfaces
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        Task AddAsync(TEntity obj);
        Task AddAsync(IClientSessionHandle session, TEntity obj);
        Task<TEntity> GetByIdAsync(string id);
        Task<TEntity> GetByIdAsync(IClientSessionHandle session, string id);
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task UpdateAsync(TEntity obj);
        Task UpdateAsync(IClientSessionHandle session, TEntity obj);
        Task<TEntity> FindOneAndReplaceAsync(IClientSessionHandle session, TEntity obj);
        Task<TEntity> FindOneAndReplaceAsync(TEntity obj);
        Task RemoveAsync(string id);
        MongoClient GetMongoClient();
    }
}