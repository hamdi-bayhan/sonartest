﻿using MongoDB.Driver;
using System;

namespace ReadingIsGood.Data.Interfaces
{
    public interface IMongoContext : IDisposable
    {
        IMongoCollection<T> GetCollection<T>(string name);
        MongoClient GetMongoClient();
    }
}