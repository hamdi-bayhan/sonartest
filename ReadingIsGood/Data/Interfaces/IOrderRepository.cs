﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ReadingIsGood.Data.Entities;

namespace ReadingIsGood.Data.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<List<Order>> GetCurrentUserOrdersAsync(string id);
    }
}