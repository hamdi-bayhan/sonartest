﻿using System.Threading.Tasks;
using ReadingIsGood.Data.Entities;

namespace ReadingIsGood.Data.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetByEmailAsync(string email);
    }
}