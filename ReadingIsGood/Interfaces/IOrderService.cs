﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ReadingIsGood.Dto.ApiRequest;
using ReadingIsGood.Dto.ApiResponse;

namespace ReadingIsGood.Interfaces
{
    public interface IOrderService
    {
        Task CreateOrderAsync(CreateOrderParams model);
        Task<OrderResponse> GetOrderDetailAsync(string id);
        Task<List<OrderResponse>> GetCurrentUserOrdersAsync();
    }
}