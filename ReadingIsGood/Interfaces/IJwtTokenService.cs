﻿using System;
using ReadingIsGood.Data.Entities;

namespace ReadingIsGood.Interfaces
{
    public interface IJwtTokenService
    {
        string GetLoginToken(User user);
    }
}
