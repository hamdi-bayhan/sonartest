﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ReadingIsGood.Dto.ApiRequest;
using ReadingIsGood.Dto.ApiResponse;

namespace ReadingIsGood.Interfaces
{
    public interface IProductService
    {
        Task<ProductResponse> UpdateStockAmountAsync(
            UpdateStockAmountParams model, string productId);
        Task<List<ProductResponse>> GetAllProductsAsync();
    }
}