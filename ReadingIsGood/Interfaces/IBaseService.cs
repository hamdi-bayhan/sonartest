﻿using ReadingIsGood.Data.Base;

namespace ReadingIsGood.Interfaces
{
    public interface IBaseService<T> where T : BaseEntity
    {
        string _CurrentUserId { get; set; }
    }
}