﻿using System.Threading.Tasks;
using ReadingIsGood.Dto.ApiRequest;
using ReadingIsGood.Dto.ApiResponse;

namespace ReadingIsGood.Interfaces
{
    public interface IUserAccountService
    {
        Task RegisterUserAsync(RegisterParams model);
        Task<LoginResponse> LoginUserAsync(LoginParams model);
        Task<UserInfoResponse> GetCurrentUserInfoAsync();
    }
}