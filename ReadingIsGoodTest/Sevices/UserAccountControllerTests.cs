using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Moq;
using Xunit;
using ReadingIsGood.Dto.ApiRequest;
using ReadingIsGood.Data.Entities;
using ReadingIsGood.Interfaces;
using ReadingIsGood.Services;
using ReadingIsGood.Data.Interfaces;
using AutoMapper;
using BC = BCrypt.Net.BCrypt;
using ReadingIsGood.Dto.ApiResponse;
using ReadingIsGood.Libraries.Exceptions;

namespace ReadingIsGoodTest.Services
{
    public class UserAccountServiceTests
    {
        private readonly Mock<IUserRepository> userRepository;
        private readonly Mock<IJwtTokenService> jwtTokenService;
        private readonly Mock<IMapper> mapper;
        private readonly Mock<ICacheService> cacheService;
        private readonly Mock<IHttpContextAccessor> httpContext;
        private readonly List<User> _users = new List<User>
        {
                new User() { Email = "user1@example.com", Name = "User1", Password = "P@ssw0rd", Age = 99, CreatedAt = DateTime.Now, Role = "Customer" },
                new User() { Email = "user2@example.com", Name = "User2", Password = "P@ssw0rd", Age = 100, CreatedAt = DateTime.Now, Role = "Customer" }
        };

        public UserAccountServiceTests()
        {
            httpContext = new Mock<IHttpContextAccessor>();
            jwtTokenService = new Mock<IJwtTokenService>();
            userRepository = new Mock<IUserRepository>();
            cacheService = new Mock<ICacheService>();
            mapper = new Mock<IMapper>();
        }

        [Fact]
        public async Task LoginUser_ValidProcess_LoginUserAsync()
        {
            var loginParams = Mock.Of<LoginParams>();
            loginParams.Email = _users[0].Email;
            loginParams.Password = _users[0].Password;

            var user = _users[0];
            user.Password = BC.HashPassword(_users[0].Password);

            // arrange
            userRepository.Setup(x => x.GetByEmailAsync(_users[0].Email)).ReturnsAsync(user);
            jwtTokenService.Setup(x => x.GetLoginToken(_users[0])).Returns("tokenToken");

            var service = new UserAccountService(httpContext.Object, userRepository.Object,
                cacheService.Object, jwtTokenService.Object, mapper.Object);

            // act
            var result = await service.LoginUserAsync(loginParams);

            //// assert
            Assert.NotNull(result);
            Assert.NotNull(result.Token);
            Assert.True(result is LoginResponse);
        }

        [Fact]
        public async Task LoginUser_WrongPasswordProcess_LoginUserAsync()
        {
            var loginParams = Mock.Of<LoginParams>();
            loginParams.Email = _users[0].Email;
            loginParams.Password = _users[0].Password;

            var user = _users[0];
            user.Password = BC.HashPassword("psswrd123");

            // arrange
            userRepository.Setup(x => x.GetByEmailAsync(_users[0].Email)).ReturnsAsync(user);

            var service = new UserAccountService(httpContext.Object, userRepository.Object,
                cacheService.Object, jwtTokenService.Object, mapper.Object);

            // act
            var exception = await Record.ExceptionAsync(() => service.LoginUserAsync(loginParams));

            //// assert
            Assert.NotNull(exception);
            Assert.IsType<BadRequestException>(exception);
        }
    }
}